import os
import sys
import unittest
#import requests
from models import *

db.create_all()

class DBTestCases(unittest.TestCase):
    def test_animal_insert_1(self):
        a = Animal(name = "Alligator", sci_name = "Alligator mississippiensis", pop_l = 70000, pop_h = 90000, pop = -1, cons_status = 4)
        db.session.add(a)
        db.session.commit()

        r = db.session.query(Animal).filter_by(name = 'Alligator').one()
        self.assertEqual(r.sci_name, 'Alligator mississippiensis')

        db.session.query(Animal).filter_by(sci_name = 'Alligator mississippiensis').delete()
        db.session.commit()

    def test_animal_insert_2(self):
        a = Animal(name = "Wolf", sci_name = "Canis Lupus", pop_l = -1, pop_h = -1, pop = 5, cons_status = 1)
        c = Country(name = "Saudi Arabia", income = 3, language = "Arabic", region = "Asia", API_ranking = 5)
        
        a.countries.append(c)
        
        db.session.add(a)
        db.session.add(c)
        db.session.commit()

        r = db.session.query(Animal).filter_by(sci_name = 'Canis Lupus').first()

        self.assertEqual(r.countries[0].name, 'Saudi Arabia')
        
        a.countries.clear()
        c.fauna.clear()
        db.session.query(Country).filter_by(name = 'Saudi Arabia').delete()
        db.session.query(Animal).filter_by(sci_name = 'Canis Lupus').delete()
        db.session.commit()

    def test_animal_insert_3(self):
        a = Animal(name = "Pangolin", sci_name = "Pholidota", pop = 3000, pop_l = -1, pop_h = -1, cons_status = 3)
        db.session.add(a)
        db.session.commit()

        duplicate = Animal(name = "Pangolin", sci_name = "Pholidota", pop = 7000, pop_l = -1, pop_h = -1, cons_status = 3)
        try:
            db.session.add(duplicate)
            self.assertEqual(1, 0)
        except:
            self.assertEqual(0, 0)
        db.session.expunge(duplicate)
        db.session.query(Animal).filter_by(sci_name = 'Pholidota').delete()
        db.session.commit()
    
    def test_country_insert_1(self):
        c = Country(name = "Georgia", language = "Georgian", income = 4, region = "Asia", API_ranking = 2)
        db.session.add(c)
        db.session.commit()

        r = db.session.query(Country).filter_by(language = 'Georgian').first()
        self.assertEqual(r.name, 'Georgia')

        db.session.query(Country).filter_by(language = 'Georgian').delete()
        db.session.commit()

    def test_country_insert_2(self):
        c = Country(name = "Kazakhstan", income = 3, language = "Russian", region = "Asia", API_ranking = 6)
        e = Ecosystem(name = "Steppe", cons_status = 4, wetland = 0, ruderal = 0, year_review = 1999, description = "Grassland")
        
        e.countries.append(c)

        db.session.add(c)
        db.session.add(e)
        db.session.commit()

        r = db.session.query(Country).filter_by(name = "Kazakhstan").first()
        self.assertEqual(r.ecos[0].name, "Steppe")
        
        c.fauna.clear()
        e.countries.clear()
        db.session.query(Country).filter_by(name = "Kazakhstan").delete()
        db.session.query(Ecosystem).filter_by(name = "Steppe").delete()
        db.session.commit()

    def test_country_insert_3(self):
        c = Country(name = "Argentina", income = 4, language = "Spanish", API_ranking = 4, region = "South America")

        try:
            db.session.add(c)
            self.assertEqual(1, 0)
        except:
            self.assertEqual(0, 0)
        db.session.expunge(c)

    def test_ecosystem_insert_1(self):
        e = Ecosystem(name = "Swamp", cons_status = 3, wetland = 1, ruderal = 1, year_review = 2003, description = "Where Shrek lives")
        db.session.add(e)
        db.session.commit()

        r = db.session.query(Ecosystem).filter_by(name = "Swamp").first()
        self.assertEqual(r.name, "Swamp")

        db.session.query(Ecosystem).filter_by(name = "Swamp").delete()
        db.session.commit()

    def test_ecosystem_insert_2(self):
        e = Ecosystem(name = "Nuclear wasteland", cons_status = 5, wetland = 0, ruderal = 1, year_review = 1981, description = "Not where you want to live")
        c = Country(name = "Sweden", income = 4, language = "Swedish", region = "Europe", API_ranking = 6)

        e.countries.append(c)

        db.session.add(e)
        db.session.add(c)
        db.session.commit()

        r = db.session.query(Ecosystem).filter_by(name = "Nuclear wasteland").first()
        self.assertEqual(r.countries[0].name, "Sweden")
        
        e.countries.clear()
        c.ecos.clear()
        db.session.query(Ecosystem).filter_by(name = "Nuclear wasteland").delete()
        db.session.query(Country).filter_by(name = "Sweden").delete()
        db.session.commit()

    def test_ecosystem_insert_3(self):
        e = Ecosystem(name = "Temperate Forest", cons_status = 4, wetland = 0, ruderal = 0, year_review = 1997, description = "Not too hot not too cold")

        try: 
            db.session.add(e)
            self.assertEqual(1, 0)
        except:
            self.assertEqual(1, 1)
        db.session.expunge(e)

if __name__ == '__main__':
    unittest.main()
