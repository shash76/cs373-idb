#!/usr/bin/env python3

# ---------------------------
# cs373-idb/flask/models.py
# Animal Safeguard
# ---------------------------


from flask import Flask
from flask_sqlalchemy import SQLAlchemy, inspect
import os
import warnings


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", 'postgresql://postgres:animalsafeguard@localhost:5432/asdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)

warnings.simplefilter("ignore")

# -------------
# Model: Animal
# -------------

class Animal(db.Model):
    """
    Animal entity in our ER model
    """
    __tablename__ = 'animal'
    sci_name = db.Column(db.String(80), primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    pop_l = db.Column(db.Integer, nullable=True)
    pop_h = db.Column(db.Integer, nullable=True)
    pop = db.Column(db.Integer, nullable=False)
    cons_status = db.Column(db.Integer, nullable=False)
    ecosystems = db.relationship('Ecosystem', secondary="anec_link", backref='fauna')
    countries = db.relationship('Country', secondary="anco_link", backref='fauna')

    def as_dict(self):
        return {'sci_name': self.sci_name, 'name': self.name, 'pop_l': self.pop_l, 'pop_h': self.pop_h, 'pop': self.pop, 'cons_status': self.cons_status, 'ecosystem': [x.name for x in self.ecosystems], 'country': [x.name for x in self.countries]} 


# ----------------
# Model: Ecosystem
# ----------------

class Ecosystem(db.Model):
    """
    Ecosystem entity in our ER model
    """
    __tablename__ = 'ecosystem'
    name = db.Column(db.String(80), primary_key=True)
    year_review = db.Column(db.Integer, nullable=False)
    ruderal = db.Column(db.Integer, nullable=False)
    wetland = db.Column(db.Integer, nullable=False)
    cons_status = db.Column(db.Integer, nullable=False)
    description = db.Column(db.String(1000), nullable=False)
    countries = db.relationship('Country', secondary="ecco_link", backref='ecos')

    def as_dict(self):
        return {'name': self.name, 'year_review': self.year_review, 'ruderal': self.ruderal, 'wetland': self.wetland, 'cons_status': self.cons_status, 'description': self.description, 'country': [x.name for x in self.countries]}


# --------------
# Model: Country
# --------------

class Country(db.Model):
    __tablename__ = 'country'
    name = db.Column(db.String(80), primary_key=True)
    language = db.Column(db.String(80), nullable=False)
    income = db.Column(db.Integer, nullable=False)
    region = db.Column(db.String(80), nullable=True)
    API_ranking = db.Column(db.Integer, nullable=False)

    def as_dict(self):
        return {'name': self.name, 'language': self.language, 'income': self.income, 'region': self.region, 'API_ranking': self.API_ranking}

anec_link = db.Table('anec_link', 
   db.Column('animal_name', db.String(80), db.ForeignKey('animal.sci_name')),
   db.Column('ecosystem_name', db.String(80), db.ForeignKey('ecosystem.name'))
)
anco_link = db.Table('anco_link', 
   db.Column('animal_name', db.String(80), db.ForeignKey('animal.sci_name')),
   db.Column('country_name', db.String(80), db.ForeignKey('country.name'))
)
ecco_link = db.Table('ecco_link', 
   db.Column('ecosystem_name', db.String(80), db.ForeignKey('ecosystem.name')),
   db.Column('country_name', db.String(80), db.ForeignKey('country.name'))
)

