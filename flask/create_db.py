# Here, we will start to populate the database

import json, ast
from models import *

# Example: dbtxt_to_dbjson("animaldata.txt", "Animals", ['name', 'pop', 'countries'])
def dbtxt_to_dbjson(filename, array_name, attributes):
    txt_file = open(filename, "r")
    animal_list = []
    for line in txt_file:
        data_pieces = line.split(">")
        animal_list.append({})
        for i in range(0, len(attributes)):
            chunk = data_pieces[i].strip()
            if chunk.startswith("[") and chunk.endswith("]"):
                chunk = ast.literal_eval(chunk)
            animal_list[-1][attributes[i]] = chunk
    txt_file.close()
    return json.dumps({array_name : animal_list})
        

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def populate_db():
    animals = json.loads(dbtxt_to_dbjson("data/animals.txt", "Animals", ['name', 'sci_name', 'pop_l', 'pop_h', 'pop', 'cons_status', 'ecosystems', 'countries']))

    for animal in animals['Animals']:
        name = animal['name']
        sci_name = animal['sci_name']
        pop_l = int(animal['pop_l'])
        pop_h = int(animal['pop_h'])
        pop = int(animal['pop'])
        cons_status = int(animal['cons_status'])
        ecosystems = animal['ecosystems']
        countries = animal['countries']

        newAnimal = Animal(name = name, sci_name = sci_name, pop_l = pop_l, pop_h = pop_h, pop = pop, cons_status = cons_status)
        # for loop - query over the countries of this animal
        # if the country in which this animal lives has not been created yet, create it
        for country in countries:
            country_result = Country.query.filter_by(name=country).first()
            if not country_result:
                # create the country
                country_result = Country(name = country, language = 'English', income = 0, region = 'NA', API_ranking = 1)
                db.session.add(country_result)
                db.session.commit()
            newAnimal.countries.append(country_result)

        for ecosystem in ecosystems:
            ecosystem_result = Ecosystem.query.filter_by(name=ecosystem).first()
            if not ecosystem_result:
                # create the ecosystem
                ecosystem_result = Ecosystem(name = ecosystem, year_review = 9999, ruderal = 0, wetland = 0, cons_status = 1, description = 'N/A')
                db.session.add(ecosystem_result)
                db.session.commit()
            newAnimal.ecosystems.append(ecosystem_result)

        db.session.add(newAnimal)
        db.session.commit()

    ecosystems = json.loads(dbtxt_to_dbjson("data/ecosystems.txt", "Ecosystems", ['name', 'year_review', 'ruderal', 'wetland', 'cons_status', 'countries', 'description']))

    for ecosystem in ecosystems['Ecosystems']:
        name = ecosystem['name']
        year_review = int(ecosystem['year_review'])
        ruderal = int(ecosystem['ruderal'])
        wetland = int(ecosystem['wetland'])
        cons_status = int(ecosystem['cons_status'])
        countries = ecosystem['countries']
        description = ecosystem['description']

        newEcosystem = Ecosystem.query.filter_by(name = name).first()
        newEcosystem.year_review = year_review
        newEcosystem.ruderal = ruderal
        newEcosystem.wetland = wetland
        newEcosystem.cons_status = cons_status
        newEcosystem.description = description

        for country in countries:
            country_result = Country.query.filter_by(name=country).first()
            if not country_result:
                # create the country
                country_result = Country(name = country, language = 'English', income = 0, region = 'NA', API_ranking = 1)
                db.session.add(country_result)
                db.session.commit()
            newEcosystem.countries.append(country_result)
        
        db.session.commit()

    countries = json.loads(dbtxt_to_dbjson("data/countries.txt", "Countries", ['name', 'language', 'income', 'region', 'API_ranking']))

    for country in countries['Countries']:
        name = country['name']
        language = country['language']
        income = int(country['income'])
        region = country['region']
        API_ranking = int(country['API_ranking'])

        newCountry = Country.query.filter_by(name = name).first()
        newCountry.language = language
        newCountry.income = income
        newCountry.region = region
        newCountry.API_ranking = API_ranking

        db.session.commit()

# populate the db
db.drop_all()
db.create_all()
populate_db()
