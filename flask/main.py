#!/usr/bin/env python3

# ---------------------------
# cs373-idb/flask/main.py
# Animal Safeguard
# ---------------------------

import os
import json
import coverage
from flask import render_template, request, redirect
from create_db import *

@app.route('/')
@app.route('/index.html')
def index():
        return render_template('index.html')

@app.route('/search')
def search():
        query = request.args.to_dict()
        searchQuery = [x.lower() for x in query['searchQuery'].split()]
        matches = [[], [], [], [], [], []]
        animals = Animal.query.all()
        for animal in animals:
            matchAtLeastOne = 0
            matchAnd = 1
            contexts = []
            for term in searchQuery:
                matchThisTerm = 0
                if (term in str(getattr(animal, 'name')).lower()):
                    matchAtLeastOne = 1
                    matchThisTerm = 1
                    contexts.append({term: str(getattr(animal, 'name'))})
                if (term in str(getattr(animal, 'sci_name')).lower()):
                    matchAtLeastOne = 1
                    matchThisTerm = 1
                    contexts.append({term: str(getattr(animal, 'sci_name'))})
                if not matchThisTerm:
                    matchAnd = 0
            result = animal.as_dict()
            result['Context'] = contexts
            if matchAnd:
                matches[0].append(result)
            elif matchAtLeastOne:
                matches[3].append(result)
        ecosystems = Ecosystem.query.all()
        for ecosystem in ecosystems:
            matchOr = 0
            matchAnd = 1
            contexts = []
            for term in searchQuery:
                if (term in str(getattr(ecosystem, 'name')).lower()):
                    matchOr = 1
                    contexts.append({term: str(getattr(ecosystem, 'name'))})
                else:
                    matchAnd = 0
            result = ecosystem.as_dict()
            result['Context'] = contexts
            if matchAnd:
                matches[1].append(result)
            elif matchOr:
                matches[4].append(result)
        countries = Country.query.all()
        for country in countries:
            matchOr = 0
            matchAnd = 1
            contexts = []
            for term in searchQuery:
                if (term in str(getattr(country, 'name')).lower()):
                    matchOr = 1
                    contexts.append({term: str(getattr(country, 'name'))})
                else:
                    matchAnd = 0
            result = country.as_dict()
            result['Context'] = contexts
            if matchAnd:
                matches[2].append(result)
            elif matchOr:
                matches[5].append(result)
        results = [{'AnimalsAnd': matches[0]}, {'EcosystemsAnd': matches[1]}, {'CountriesAnd': matches[2]}, {'AnimalsOr': matches[3]}, {'EcosystemsOr': matches[4]}, {'CountriesOr': matches[5]}]
        return json.dumps({'Results': results})

@app.route('/results')
def results():
        os.system('coverage run --branch test.py > results.txt 2>&1')
        os.system('coverage report --include=test.py >> results.txt')
        tokens = open('results.txt').read()
        return tokens

@app.route('/about.html')
def about():
        #os.system('coverage run --branch test.py > results.txt 2>&1')
        #os.system('coverage report -m >> results.txt')
        #tokens = open('results.txt').read()
        return "Passing data from flask"

@app.route('/animals/<animal>')
def animal(animal):
        return render_template('index.html', tokens = animal)

@app.route('/ecosystems/<eco>')
def eco(eco):
        return render_template('index.html', tokens = eco)

@app.route('/countries/<country>')
def country(country):
        return render_template('index.html', tokens = country)

@app.route('/api/all_animals')
def all_animals():
        data = request.args.to_dict()
        animals = Animal.query.all()
        try:
            if 'sort' in data:
                sortKey = data['sortKey']
                animals.sort(key = lambda x: getattr(x, sortKey))
                if (data['sort'] == 'rev'):
                    animals.reverse()
        except:
            return "Error - sort not called successfully"
        return json.dumps({'Animals': [x.as_dict() for x in animals]})

@app.route('/api/all_countries')
def all_countries():
        data = request.args.to_dict()
        countries = Country.query.all()
        try:
            if 'sort' in data:
                sortKey = data['sortKey']
                countries.sort(key = lambda x: getattr(x, sortKey))
                if (data['sort'] == 'rev'):
                    countries.reverse()
        except:
            return "Error - sort not called successfully"
        return json.dumps({'Countries': [x.as_dict() for x in countries]})

@app.route('/api/all_ecosystems')
def all_ecosystems():
        data = request.args.to_dict()
        ecosystems = Ecosystem.query.all()
        try:
            if 'sort' in data:
                sortKey = data['sortKey']
                ecosystems.sort(key = lambda x: getattr(x, sortKey))
                if (data['sort'] == 'rev'):
                    ecosystems.reverse()
        except:
            return "Error - sort not called successfully"
        return json.dumps({'Ecosystems': [x.as_dict() for x in ecosystems]})

@app.route('/api/animal')
def animal_query():
        data = request.args.to_dict()
        animals = Animal.query.all()
        matches = []
        translator = {'country': 'countries', 'ecosystem': 'ecosystems'}
        for animal in animals:
            match = 1
            for arg in data:
                attr = arg
                if arg in translator:
                    attr = translator[arg]
                try:
                    if (attr == 'countries' or attr == 'ecosystems'):
                        if not (data[arg] in [x.name for x in getattr(animal, attr)]):
                            match = 0
                            break 
                    elif (str(getattr(animal, attr)) != data[arg]):
                        match = 0
                        break
                except:
                    return "Error - invalid attribute"
            if match:
                matches.append(animal.as_dict())
        try:
            if 'sort' in data:
                sortKey = data['sortKey']
                matches.sort(key = lambda x: x[sortKey])
                if (data['sort'] == 'rev'):
                    matches.reverse()
        except:
            return "Error - sort not called successfully"
        return json.dumps({'Animals': matches})

@app.route('/api/ecosystem')
def ecosystem_query():
        data = request.args.to_dict()
        ecosystems = Ecosystem.query.all()
        matches = []
        translator = {'country': 'countries'}
        for ecosystem in ecosystems:
            match = 1
            for arg in data:
                attr = arg
                if arg in translator:
                    attr = translator[arg]
                try:
                    if (attr == 'countries'):
                        if not (data[arg] in [x.name for x in getattr(ecosystem, attr)]):
                            match = 0
                            break
                    elif (str(getattr(ecosystem, attr)) != data[arg]):
                        match = 0
                        break
                except:
                    return "Error - invalid attribute"
            if match:
                matches.append(ecosystem.as_dict())
        try:
            if 'sort' in data:
                sortKey = data['sortKey']
                matches.sort(key = lambda x: x[sortKey])
                if (data['sort'] == 'rev'):
                    matches.reverse()
        except:
            return "Error - sort not called successfully"
        return json.dumps({'Ecosystem': matches})

@app.route('/api/country')
def country_query():
        data = request.args.to_dict()
        countries = Country.query.all()
        matches = []
        for country in countries:
            match = 1
            for arg in data:
                try:
                    if (str(getattr(country, arg)) != data[arg]):
                        match = 0
                        break
                except:
                    return "Error - invalid attribute"
            if match:
                matches.append(country.as_dict())
        try:
            if 'sort' in data:
                sortKey = data['sortKey']
                matches.sort(key = lambda x: x[sortKey])
                if (data['sort'] == 'rev'):
                    matches.reverse()
        except:
            return "Error - sort not called successfully"
        return json.dumps({'Country': matches})
        

if __name__ == "__main__":
        app.run(host = '0.0.0.0')

