import React from 'react';
import "./map.css";
import USAMap from "react-usa-map";
import BarChart from 'react-easy-bar-chart';
import BubbleChart from "@weknow/react-bubble-chart-d3";
import DemographicsList from "./demo.json";
import ShelterList from "./shel.json";
import FoodList from "./food.json";

var shelPieList = [];
var foodBubbleList = [];

class Other extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            pops: [],
        }
        //this.getAPIData();
        this.sheltersByProvider();
        this.foodByState();
    }

    getAPIData(){
        /*const APIurl = "http://helpinghomeless.me/api/demographics?count_type=Overall%20Homeless";
        //const APIurl = "https://my-json-server.typicode.com/shash76/demographicsJSON/db";
        fetch(APIurl, {mode: "no-cors", headers: {'Access-Control-Allow-Origin': '*'}, method: "GET"}) //call the API
        .then(response => console.log(response));
        .then(response => response.json() //turn content into JSON
        .then(data => this.setState({pops: data})); //pass JSON data into pops array in state*/
        //fetch(APIurl, {method: "GET"}) //call the API
        //fetch(APIurl, {mode: "no-cors", method: "GET"}) //call the API

        /*fetch(APIurl, {mode: "no-cors", headers: {'Access-Control-Allow-Origin': '*'}, method: "GET"}) //call the API
          .then(response => response.text())
          .then(response => console.log(response));
          .then((data) => Promise.resolve(data ? JSON.parse(data) : {}))
          .then((data) => this.setState({pops: data}))
          .catch((error) => Promise.reject(error));*/

          /*fetch(APIurl, {
            mode: 'no-cors',
            method: 'GET',
            headers: {
              Accept: 'application/json',
            },
          },
          ).then(response => {
            if (response.ok) {
              response.json().then(json => {
                console.log(json);
              });
            }
          });*/
    }

    mapHandler = (event) => {
        alert(event.target.dataset.name);
    };

    statesCustomConfig = () => {
        //let stateName = "AK";
        let popData = DemographicsList;
        return {
          //stateName.toString(): {
          "AK": {
            fill: popData[0].overall < 10000 ? "green" : (popData[0].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Alaska's Homeless Population: " + popData[0].overall)
          },
          "AL": {
            fill: popData[1].overall < 10000 ? "green" : (popData[1].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Alabama's Homeless Population: " + popData[1].overall)
          },
          "AR": {
            fill: popData[2].overall < 10000 ? "green" : (popData[2].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Arkansas' Homeless Population: " + popData[2].overall)
          },
          "AZ": {
            fill: popData[3].overall < 10000 ? "green" : (popData[3].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Arizona's Homeless Population: " + popData[3].overall)
          },
          "CA": {
            fill: popData[4].overall < 10000 ? "green" : (popData[4].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("California's Homeless Population: " + popData[4].overall)
          },
          "CO": {
            fill: popData[5].overall < 10000 ? "green" : (popData[5].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Colorado's Homeless Population: " + popData[5].overall)
          },
          "CT": {
            fill: popData[6].overall < 10000 ? "green" : (popData[6].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Connecticut's Homeless Population: " + popData[6].overall)
          },
          "DC": {
            fill: popData[7].overall < 10000 ? "green" : (popData[7].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Washington DC's Homeless Population: " + popData[7].overall)
          },
          "DC2": {
            fill: popData[7].overall < 10000 ? "green" : (popData[7].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Washington DC's Homeless Population: " + popData[7].overall)
          },
          "DE": {
            fill: popData[8].overall < 10000 ? "green" : (popData[8].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Deleware's Homeless Population: " + popData[8].overall)
          },
          "FL": {
            fill: popData[9].overall < 10000 ? "green" : (popData[9].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Florida's Homeless Population: " + popData[9].overall)
          },
          "GA": {
            fill: popData[10].overall < 10000 ? "green" : (popData[10].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Georgia's Homeless Population: " + popData[10].overall)
          },
          "HI": {
            fill: popData[12].overall < 10000 ? "green" : (popData[12].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Hawaii's Homeless Population: " + popData[12].overall)
          },
          "IA": {
            fill: popData[13].overall < 10000 ? "green" : (popData[13].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Iowa's Homeless Population: " + popData[13].overall)
          },
          "ID": {
            fill: popData[14].overall < 10000 ? "green" : (popData[14].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Idaho's Homeless Population: " + popData[14].overall)
          },
          "IL": {
            fill: popData[15].overall < 10000 ? "green" : (popData[15].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Illinois' Homeless Population: " + popData[15].overall)
          },
          "IN": {
            fill: popData[16].overall < 10000 ? "green" : (popData[16].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Indiana's Homeless Population: " + popData[16].overall)
          },
          "KS": {
            fill: popData[17].overall < 10000 ? "green" : (popData[17].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Kansas' Homeless Population: " + popData[17].overall)
          },
          "KY": {
            fill: popData[18].overall < 10000 ? "green" : (popData[18].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Kentucky's Homeless Population: " + popData[18].overall)
          },
          "LA": {
            fill: popData[19].overall < 10000 ? "green" : (popData[19].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Louisiana's Homeless Population: " + popData[19].overall)
          },
          "MA": {
            fill: popData[20].overall < 10000 ? "green" : (popData[20].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Massachusetts' Homeless Population: " + popData[20].overall)
          },
          "MD": {
            fill: popData[21].overall < 10000 ? "green" : (popData[21].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Maryland's Homeless Population: " + popData[21].overall)
          },
          "ME": {
            fill: popData[22].overall < 10000 ? "green" : (popData[22].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Maine's Homeless Population: " + popData[22].overall)
          },
          "MI": {
            fill: popData[23].overall < 10000 ? "green" : (popData[23].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Michigan's Homeless Population: " + popData[23].overall)
          },
          "MN": {
            fill: popData[24].overall < 10000 ? "green" : (popData[24].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Minnesota's Homeless Population: " + popData[24].overall)
          },
          "MO": {
            fill: popData[25].overall < 10000 ? "green" : (popData[25].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Missouri's Homeless Population: " + popData[25].overall)
          },
          "MS": {
            fill: popData[27].overall < 10000 ? "green" : (popData[27].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Mississippi's Homeless Population: " + popData[27].overall)
          },
          "MT": {
            fill: popData[28].overall < 10000 ? "green" : (popData[28].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Montana's Homeless Population: " + popData[28].overall)
          },
          "NC": {
            fill: popData[29].overall < 10000 ? "green" : (popData[29].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("North Carolina's Homeless Population: " + popData[29].overall)
          },
          "ND": {
            fill: popData[30].overall < 10000 ? "green" : (popData[30].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("North Dakota's Homeless Population: " + popData[30].overall)
          },
          "NE": {
            fill: popData[31].overall < 10000 ? "green" : (popData[31].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Nebraska's Homeless Population: " + popData[31].overall)
          },
          "NH": {
            fill: popData[32].overall < 10000 ? "green" : (popData[32].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("New Hampshire's Homeless Population: " + popData[32].overall)
          },
          "NJ": {
            fill: popData[33].overall < 10000 ? "green" : (popData[33].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("New Jersey's Homeless Population: " + popData[33].overall)
          },
          "NM": {
            fill: popData[34].overall < 10000 ? "green" : (popData[34].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("New Mexico's Homeless Population: " + popData[34].overall)
          },
          "NV": {
            fill: popData[35].overall < 10000 ? "green" : (popData[35].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Nevada's Homeless Population: " + popData[35].overall)
          },
          "NY": {
            fill: popData[36].overall < 10000 ? "green" : (popData[36].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("New York's Homeless Population: " + popData[36].overall)
          },
          "OH": {
            fill: popData[37].overall < 10000 ? "green" : (popData[37].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Ohio's Homeless Population: " + popData[37].overall)
          },
          "OK": {
            fill: popData[38].overall < 10000 ? "green" : (popData[38].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Oklahoma's Homeless Population: " + popData[38].overall)
          },
          "OR": {
            fill: popData[39].overall < 10000 ? "green" : (popData[39].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Oregon's Homeless Population: " + popData[39].overall)
          },
          "PA": {
            fill: popData[40].overall < 10000 ? "green" : (popData[40].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Pennslyvania's Homeless Population: " + popData[40].overall)
          },
          "RI": {
            fill: popData[42].overall < 10000 ? "green" : (popData[42].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Rhode Island's Homeless Population: " + popData[42].overall)
          },
          "SC": {
            fill: popData[43].overall < 10000 ? "green" : (popData[43].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("South Carolina's Homeless Population: " + popData[43].overall)
          },
          "SD": {
            fill: popData[44].overall < 10000 ? "green" : (popData[44].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("South Dakota's Homeless Population: " + popData[44].overall)
          },
          "TN": {
            fill: popData[45].overall < 10000 ? "green" : (popData[45].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Tennessee's Homeless Population: " + popData[45].overall)
          },
          "TX": {
            fill: popData[46].overall < 10000 ? "green" : (popData[46].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Texas' Homeless Population: " + popData[46].overall)
          },
          "UT": {
            fill: popData[47].overall < 10000 ? "green" : (popData[47].overall < 20000 ? "#ffee0a" : "red"),
            clickHandler: () => alert("Utah's Homeless Population: " + popData[47].overall)
          }
        };
    };

    sheltersByProvider() {
      let shelterList = ShelterList;
      var provDict = {};
      for (var shelterIdx = 0; shelterIdx < shelterList.length; shelterIdx++) {
        if (shelterList[shelterIdx].provider in provDict) {
          provDict[shelterList[shelterIdx].provider]++;
        }
        else {
          provDict[shelterList[shelterIdx].provider] = 1;
        }
      }
      var tempList = [provDict.length];
      var idx = 0
      for (var key in provDict) {
        tempList[idx] = {title: key, value: provDict[key], color:"#104f96"};
        idx++;
      }
      shelPieList = tempList;
    }

    foodByState() {
      let foodList = FoodList;
      var foodDict = {};
      for (var foodIdx = 0; foodIdx < foodList.length; foodIdx++) {
        if (foodList[foodIdx].stateOrProvince in foodDict) {
          foodDict[foodList[foodIdx].stateOrProvince]++;
        }
        else {
          foodDict[foodList[foodIdx].stateOrProvince] = 1;
        }
      }
      var tempList = [foodDict.length];
      var idx = 0
      for (var key in foodDict) {
        tempList[idx] = {label: key, value: foodDict[key]};
        idx++;
      }
      foodBubbleList = tempList;
    }

    render() {
      console.log(shelPieList);
        return (
            <main role="main" class="inner cover">
                <div className="opaque">
                    <h1 className="cover-heading">Data Visualization</h1>
                    <p className="lead">Here you will find different forms of visualizing various types of data,
                    including overall homeless populations, shelter providers, and food resources.</p>
                    <p>Group 6 made an API that houses a plethora of data designed to document and alleviate the
                      growing problem of homelessness in the United States. We used the data for all three of their models
                      in their API to create three distinct visualizations that we hope will inspire others to fight for a
                      better future for all by ending homelessness in this country.
                    </p>
                </div>
                <div class="opaque">
                    <h1 class="cover-heading">Map of U.S. Homeless Populations</h1>
                    <p align="center">Green - &lt;10,000 Overall Homeless Population</p>
                    <p align="center">Yellow - &lt;20,000 Overall Homeless Population</p>
                    <p align="center">Red - &ge;20,000 Overall Homeless Population</p>
                    <p align="center">Gray - Data Not Available for State</p>
                    <p align="center">Click on each state for the Exact Overall Homeless Population</p>
                    <div className="App">
                        <USAMap customize={this.statesCustomConfig()} width={"47em"} onClick={this.mapHandler} />
                    </div>
                </div>
                <div class="opaque">
                    <h1 class="cover-heading">Bar Graph of Homeless Shelter Providers</h1>
                    <p align="center">Hover over each bar to see Name of Provider and the Number of Shelters</p>
                    <div className="App" align="center">
                        <BarChart
                          xAxis="Provider Names"
                          yAxis="Number of Homeless Shelters"
                          height={400}
                          width={850}
                          data={shelPieList}/>
                    </div>
                </div>
                <div class="opaque">
                    <h1 class="cover-heading">Bubble Graph of US States by Number of Food Resource Centers</h1>
                    <div className="App">
                        <BubbleChart graph= {{
                        zoom: 1,
                        offsetX: -0.00,
                        offsetY: -0.00,
                      }}
                      width={850}
                      //width={"50em"}
                      height={700}
                      padding={0} // optional value, number that set the padding between bubbles
                      showLegend={true} // optional value, pass false to disable the legend.
                      legendPercentage={20} // number that represent the % of with that legend going to use.
                      legendFont={{
                            family: 'Arial',
                            size: 12,
                            color: '#000',
                            weight: 'bold',
                          }}
                      valueFont={{
                            family: 'Arial',
                            size: 12,
                            color: '#fff',
                            weight: 'bold',
                          }}
                      labelFont={{
                            family: 'Arial',
                            size: 16,
                            color: '#fff',
                            weight: 'bold',
                          }}
                      //Custom bubble/legend click functions such as searching using the label, redirecting to other page
                      bubbleClickFunc={this.bubbleClick}
                      legendClickFun={this.legendClick}
                      data={foodBubbleList}/>
                    </div>
                </div>
            </main>
        );
    }
}

export default Other;