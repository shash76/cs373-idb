import React from 'react';

class CounEntry extends React.Component{
    constructor(props){
        super(props);
        this.name = new URLSearchParams(window.location.search);
        this.name = window.location.href.slice(window.location.href.indexOf('entry/') + 'entry/'.length);
        this.imgName = this.name.toString().toLowerCase().replace(/%20/g, "_").replace(/%27/g, "");
        this.state = {
            country: []
        }
        this.getAPIData();
    }

    getAPIData(){
        const APIurl = "/api/country?name=" + this.name;
        fetch(APIurl, {
            method: "GET"
        }) //call the API
        .then(response => response.json()) //turn content into JSON
        .then(data => this.setState({country: data.Country[0]}));
    }

    render() {
        let counData = this.state.country;
        let imgName = this.imgName;
        return (
            <main role="main" class="inner cover">
                <h1 class="cover-heading">{counData.name}</h1>
                <div class="card mb-3">
                    <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src={process.env.PUBLIC_URL + `/images/${imgName}.jpg`} class="card-img" alt="Country Flag"></img>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                        <h5 class="card-title">{counData.name}</h5>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><b>Name: </b>{counData.name}</li>
                            <li class="list-group-item"><b>Region: </b>{counData.region}</li>
                            <li class="list-group-item"><b>Income Level: </b>{counData.income}</li>
                            <li class="list-group-item"><b>Native Language: </b>{counData.language}</li>
                            <li class="list-group-item"><b>API Ranking: </b>{counData.API_ranking}</li>
                        </ul>
                        </div>
                    </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default CounEntry;