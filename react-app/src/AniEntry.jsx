import React from 'react';
import { NavLink } from "react-router-dom";

class AniEntry extends React.Component{
    constructor(props){
        super(props);
        this.name = new URLSearchParams(window.location.search);
        this.name = window.location.href.slice(window.location.href.indexOf('entry/') + 'entry/'.length);
        this.imgName = this.name.toString().toLowerCase().replace(/%20/g, "_").replace(/%27/g, "");
        this.state = {
            animal: []
        }
        this.getAPIData();
    }

    getAPIData(){
        const APIurl = "/api/animal?name=" + this.name;
        fetch(APIurl, {
            method: "GET"
        }) //call the API
        .then(response => response.json()) //turn content into JSON
        .then(data => this.setState({animal: data.Animals[0]})); //pass JSON data into animals array in state
    }

    render() {
        let aniData = this.state.animal;
        let imgName = this.imgName;
        return (
            <main role="main" class="inner cover">
                <h1 class="cover-heading">{aniData.name}</h1>
                <div class="card mb-3">
                    <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src={process.env.PUBLIC_URL + `/images/${imgName}.jpg`} class="card-img" alt="Animal"></img>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                        <h5 class="card-title">{aniData.sci_name}</h5>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><b>Main Location: </b>
                            {aniData.country ? aniData.country.map(entry => {return(
                            <NavLink className = "nav-link" to={"/countries/entry/" + entry}>{entry}</NavLink>
                            );}) : null}
                            </li>
                            <li class="list-group-item"><b>Habitat: </b>
                            {aniData.ecosystem ? aniData.ecosystem.map(entry => {return(
                            <NavLink className = "nav-link" to={"/ecosystems/entry/" + entry}>{entry}</NavLink>
                            );}) : null}
                            </li>
                            <li class="list-group-item"><b>Threat Level: </b>{aniData.cons_status}</li>
                            <li class="list-group-item"><b>Population: </b>{aniData.pop == -1 ? (aniData.pop_l + aniData.pop_h)/2: aniData.pop}</li>
                        </ul>
                        </div>
                    </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default AniEntry;
