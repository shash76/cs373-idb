import React, { Component } from "react";
import {
    Route,
    NavLink,
    HashRouter
  } from "react-router-dom";
  import Home from "./Home";
  import Animals from "./Animals";
  import AniEntry from "./AniEntry";
  import Countries from "./Countries";
  import CounEntry from "./CounEntry";
  import Ecosystems from "./Ecosystems";
  import EcoEntry from "./EcoEntry";
  import Other from "./Other";
  import About from "./About";
  import Search from "./Search";
 
class Main extends Component {
  render() {
    return (
      <HashRouter>
        <div className="cover-container d-flex w-100 centered-welcome p-3 mx-auto flex-column">
          <header className="masthead mb-auto">
          <div className="inner">
            <h3 className="masthead-brand">Animal Safeguard</h3>
            <nav className="nav nav-masthead justify-content-center">
              <NavLink className="nav-link" exact to="/">Home</NavLink>
              <NavLink className="nav-link" to="/animals">Animals</NavLink>
              <NavLink className="nav-link" to="/countries">Countries</NavLink>
              <NavLink className="nav-link" to="/ecosystems">Ecosystems</NavLink>
              <NavLink className="nav-link" to="/other">Other</NavLink>
              <NavLink className="nav-link" to="/about">About</NavLink>
            </nav>
          </div>
          </header>

          <div className="content">
            <Route exact path="/" component={Home}/>
            <Route exact path="/animals" component={Animals}/>
            <Route path="/animals/entry" component={AniEntry}/>
            <Route exact path="/countries" component={Countries}/>
            <Route path="/countries/entry" component={CounEntry}/>
            <Route exact path="/ecosystems" component={Ecosystems}/>
            <Route path="/ecosystems/entry" component={EcoEntry}/>
            <Route path="/other" component={Other}/>
            <Route path="/about" component={About}/>
            <Route path="/search-result" component={Search}/>
          </div>
        </div>
      </HashRouter>
    );
  }
}
 
export default Main;