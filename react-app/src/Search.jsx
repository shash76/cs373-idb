import React from 'react';
import { NavLink } from "react-router-dom";

class Search extends React.Component{
    constructor(props){
        super(props);
        this.name = new URLSearchParams(window.location.search);
        this.name = window.location.href.slice(window.location.href.indexOf('search-result/') + 'search-result/'.length);
        this.state = {
            results: []
        }
        this.getSearchData();
    }

    getSearchData(){
        const APIurl = "/search?searchQuery=" + this.name;
        fetch(APIurl, {
            method: "GET"
        }) //call the API
        .then(response => response.json()) //turn content into JSON
        .then(data => this.setState({results: data.Results})); //pass JSON data into animals array in state
    }

    render() {
        let searchData = this.state.results;
        let animalsAnd = [];
        let ecosystemsAnd = [];
        let countriesAnd = [];
        let animalsOr = [];
        let ecosystemsOr = [];
        let countriesOr = [];
        if(searchData.length > 0){
            animalsAnd = searchData[0].AnimalsAnd;
            ecosystemsAnd = searchData[1].EcosystemsAnd;
            countriesAnd = searchData[2].CountriesAnd;
            animalsOr = searchData[3].AnimalsOr;
            ecosystemsOr = searchData[4].EcosystemsOr;
            countriesOr = searchData[5].CountriesOr;
        }
        console.log(searchData)
        return (
            <main role="main" class="inner cover">
                {/*only display AND results if there are any to display */
                animalsAnd.length === 0 && ecosystemsAnd.length === 0 && countriesAnd === 0 ? null : <h1>AND Results</h1>
                }
                {/*only display Animals results if there are any to display */
                animalsAnd.length === 0 ? null: 
                <div class="card mb-3">
                    <h2>Animals</h2>
                    {animalsAnd.map(entry => {return(
                        <div>
                            <NavLink class="nav-link" to={"/animals/entry/" + entry.name}>{entry.name} (<i>{entry.sci_name}</i>)</NavLink>
                        </div>
                    );})}
                </div>
                }
                {/*only display Ecosystem results if there are any to display */
                ecosystemsAnd.length === 0 ? null:
                <div class="card mb-3">
                    <h2>Ecosystems</h2>
                    {ecosystemsAnd.map(entry => {return(
                        <NavLink class="nav-link" to={"/ecosystems/entry/" + entry.name}>{entry.name}</NavLink>
                    );})}
                </div>
                }
                {/*only display Countries results if there are any to display */
                countriesAnd.length === 0 ? null :
                <div class="card mb-3">
                    <h2>Countries</h2>
                    {countriesAnd.map(entry => {return(
                        <NavLink class="nav-link" to={"/countries/entry/" + entry.name}>{entry.name}</NavLink>
                    );})}
                </div>
                }
                

                {/*only display OR results if there are any to display */
                animalsOr.length === 0 && ecosystemsOr.length === 0 && countriesOr.length === 0 ? null : <h1>OR Results</h1>
                }
                {/*only display Animals results if there are any to display */
                animalsOr.length === 0 ? null: 
                <div class="card mb-3">
                    <h2>Animals</h2>
                    {animalsOr.map(entry => {return(
                        <div>
                            <NavLink class="nav-link" to={"/animals/entry/" + entry.name}>{entry.name} (<i>{entry.sci_name}</i>)</NavLink>
                        </div>
                    );})}
                </div>
                }
                {/*only display Ecosystem results if there are any to display */
                ecosystemsOr.length === 0 ? null:
                <div class="card mb-3">
                    <h2>Ecosystems</h2>
                    {ecosystemsOr.map(entry => {return(
                        <NavLink class="nav-link" to={"/ecosystems/entry/" + entry.name}>{entry.name}</NavLink>
                    );})}
                </div>
                }
                {/*only display Countries results if there are any to display */
                countriesOr.length === 0 ? null :
                <div class="card mb-3">
                    <h2>Countries</h2>
                    {countriesOr.map(entry => {return(
                        <NavLink class="nav-link" to={"/countries/entry/" + entry.name}>{entry.name}</NavLink>
                    );})}
                </div>
                }
                
            </main>
        );
    }
}

export default Search;