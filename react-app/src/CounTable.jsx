import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';


class CounTable extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            countries: []
        }
        this.getAPIData();
    }

    getAPIData(){
        //dummy url; NOTE TO SELF: come back and change later!
        const APIurl = "http:/api/all_countries"; //"https://my-json-server.typicode.com/ayaabdelgawad/cs373-idb/db";
        fetch(APIurl, {
            method: "GET"
        }) //call the API
        .then(response => response.json()) //turn content into JSON
        .then(data => {this.setState({countries: data.Countries})}); //pass JSON data into countries array in state
    }

    render() {
        let countriesData = this.state.countries;
        return(
            
            <TableContainer component={Paper}>
            <Table >
                <TableHead>
                <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Region</TableCell>
                    <TableCell>Income Level</TableCell>
                    <TableCell>Native Language</TableCell>
                    <TableCell>API Ranking</TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {countriesData.map((row) => (
                    <TableRow key={row.region}>
                    <TableCell component="th" scope="row">
                        {row.name}
                    </TableCell>
                    <TableCell>{row.region}</TableCell>
                    <TableCell>{row.income}</TableCell>
                    <TableCell>{row.language}</TableCell>
                    <TableCell>{row.API_ranking}</TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
            </TableContainer>
        );
    }
}

export default CounTable;