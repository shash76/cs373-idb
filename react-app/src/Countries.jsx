import React from 'react';
import CounEnhTable from './CounEnhTable';

class Countries extends React.Component {
    render() {
        return (
            <main role="main" class="inner cover">
            <div class="opaque">
                <h1 class="cover-heading">Countries</h1>
                <p class="lead">Here you will find information about different countries. Most important to note is the 
                Animal Protection Index ranking based off of each country's animal welfare policy and legislation.</p>
            </div>
            <CounEnhTable/>
            </main>
        );
    }
}

export default Countries;