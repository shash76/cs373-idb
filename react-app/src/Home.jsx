import React, { Component } from "react";
import {NavLink, HashRouter} from 'react-router-dom';
 
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render() {
    return (
      <HashRouter>
      <main className="inner cover">
        <div className="opaque">
        <h1 className="cover-heading">Welcome to Animal Safeguard!</h1>
        <form onSubmit={this.handleSubmit} className="input-group" method="GET">
          <input value={this.state.value} onChange={this.handleChange} className="form-control width100" type="text" placeholder="Start Exploring"/>
          <div class="input-group-append">
            <NavLink to={"/search-result/" + this.state.value} class="btn btn-outline-secondary" type="submit">Search</NavLink>
          </div>
        </form>
        <p className="lead">Our mission is to inform you about threatened and endangered animals. This
        website will give you vital information about what animals are at risk of extinction, where they
        can be found, and what their ecosystems are like. Our hope is that you use this information to
        make yourself aware of the threat to animals all around the world and to find ways to contribute
        to conservation efforts that will allow these animals to thrive for generations to come.</p>
        </div>
      </main>
      </HashRouter>
    );
  }
}
 
export default Home;