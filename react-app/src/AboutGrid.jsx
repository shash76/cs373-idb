import React from 'react';
import Grid from '@material-ui/core/Grid';

class AboutGrid extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {
            output: ""
        }
        this.getAPIData();
    }
    
    getAPIData(){
        fetch("/results", {
            method: "GET"}) //call the API
            .then(response => response.text()).then(data => {this.setState({output: data})});
    }

    render(){
        return(
            <div >
      <Grid container spacing={3}>
        <Grid item xs={12}>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src={process.env.PUBLIC_URL + '/images/jorge-garza.jpg'} class="card-img" alt="J. Garza Headshot"/>
                    </div>
                    <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Jorge Garza</h5>
                        <p class="card-text">UT ‘20 CS graduate. Originally from Monterrey, Mexico. 
                            Incoming Systems Engineer at Cisco</p>
                        <p class="card-text">Responsibilities: Focusing on backend and database management. 
                            Looking forward to learning how to design and implement APIs!</p>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item">No. of Commits: 45</li>
                        <li class="list-group-item">No. of Issues: 19</li>
                        <li class="list-group-item">No. of Unit Tests: 4</li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
        </Grid>
        <Grid item xs={12}>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src={process.env.PUBLIC_URL + '/images/aya-abdelgawad.jpg'} class="card-img" alt="A. Abdelgawad Headshot"/>
                    </div>
                    <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Aya Abdelgawad</h5>
                        <p class="card-text">UT CS student with math concentration</p>
                        <p class="card-text">Responsibilities: GUI</p>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item">No. of Commits: 91</li>
                        <li class="list-group-item">No. of Issues: 16</li>
                        <li class="list-group-item">No. of Unit Tests: 0</li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
        </Grid>
        <Grid item xs={12}>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src={process.env.PUBLIC_URL + '/images/shashank-bhavimane.jpg'} class="card-img" alt="S. Bhavimane Headshot"/>
                    </div>
                    <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Shashank Bhavimane</h5>
                        <p class="card-text">UT CS Class of '22. From North Texas.</p>
                        <p class="card-text">Responsibilities: Focusing on designing the GUI and working on 
                            the front end.</p>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item">No. of Commits: 87</li>
                        <li class="list-group-item">No. of Issues: 15</li>
                        <li class="list-group-item">No. of Unit Tests: 0</li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
        </Grid>
        <Grid item xs={12}>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src={process.env.PUBLIC_URL + '/images/carrie-lu.jpg'} class="card-img" alt="C. Lu Headshot"/>
                    </div>
                    <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Carrie Lu</h5>
                        <p class="card-text">UT CS Class of '21. From Keller, TX.</p>
                        <p class="card-text">Responsibilities: Focusing on GUI and front end implementation.</p>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item">No. of Commits: 88</li>
                        <li class="list-group-item">No. of Issues: 10</li>
                        <li class="list-group-item">No. of Unit Tests: 0</li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
        </Grid>
        <Grid item xs={12}>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src={process.env.PUBLIC_URL + '/images/zachary-king.jpg'} class="card-img" alt="Z. King Headshot"/>
                    </div>
                    <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Zachary King</h5>
                        <p class="card-text">UT CS student, class of 2021. Local Austinite.</p>
                        <p class="card-text">Responsibilities: Backend/API/database</p>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item">No. of Commits: 0</li>
                        <li class="list-group-item">No. of Issues: 18</li>
                        <li class="list-group-item">No. of Unit Tests: 5</li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
        </Grid>
        <Grid item xs={12}>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="card-body">
                        <h5 class="card-title">Team Statistics</h5>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item"><b>Total No. of Commits: </b>311</li>
                        <li class="list-group-item"><b>Total No. of Issues: </b>63</li>
                        <li class="list-group-item"><b>Total No. of Unit Tests: </b>9</li>
                        <li class="list-group-item">
                            <a href="https://documenter.getpostman.com/view/11831273/T17J7mhq?version=latest">Our Postman API</a>
                        </li>
                        <li class="list-group-item">
                            <a href="https://gitlab.com/shash76/cs373-idb/-/issues">GitLab Issue Tracker &emsp;</a>
                            <a href="https://gitlab.com/shash76/cs373-idb">GitLab Repository &emsp;</a>
                            <a href="https://gitlab.com/shash76/cs373-idb/-/wikis/0.-Overview">GitLab Wiki</a>
                        </li>
                        <li class="list-group-item">
                            <a href="https://speakerdeck.com/animalsafeguard/animal-safeguard-cs-373-presentation-d7dc771a-d2d2-45e7-8f83-3da50a6b70a1">Our Speaker Deck</a>
                        </li>
                        </ul>
                    </div>
                </div>
            </div>
        </Grid>
        <Grid item xs={12}>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="card-body">
                        <h5 class="card-title">Data Sources</h5>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item"><b></b>
                            <a href="https://explorer.natureserve.org/api-docs/#_species_search">Nature Serve API</a>
                            <p>This API was used to gather information about the ecoystems on the website.</p>
                        </li>
                        <li class="list-group-item"><b></b>
                            <a href="http://www.bloowatch.org/developers/api">Bloowatch API</a>
                            <p>This API was used to gather information about the animals on the website.</p>
                        </li>
                        <li class="list-group-item"><b></b>
                            <a href="https://fabian7593.github.io/CountryAPI/#what-is-countryapi">Country API</a>
                            <p>This API was used to gather information about the countries on the website.</p>
                        </li>
                        <li class="list-group-item"><b></b>
                            <a href="https://datahelpdesk.worldbank.org/knowledgebase/articles/898590-country-api-queries">World Bank Country API</a>
                            <p>This API was used to gather additional information about the countries on the website.</p>
                        </li>  
                        </ul>
                    </div>
                </div>
            </div>
        </Grid>
        <Grid item xs={12}>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="card-body">
                        <h5 class="card-title">Tools</h5>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a href="https://about.gitlab.com/">
                                &emsp;
                                <img src={process.env.PUBLIC_URL + '/images/gitlab-logo.jpg'} alt="GitLab Logo" width={50} height={50}/>
                                &emsp;
                            </a>
                            <a href="https://getbootstrap.com/">
                                &emsp;
                                <img src={process.env.PUBLIC_URL + '/images/bootstrap-logo.jpg'} alt="Bootstrap Logo" width={50} height={50}/>
                                &emsp;
                            </a>
                            <a href="https://cloud.google.com/">
                                &emsp;
                                <img src={process.env.PUBLIC_URL + '/images/gcp-logo.jpg'} alt="GCP Logo" width={50} height={50}/>
                                &emsp;
                            </a>
                            <a href="https://reactjs.org/">
                                &emsp;
                                <img src={process.env.PUBLIC_URL + '/images/react-logo.jpg'} alt="React Logo" width={50} height={50}/>
                                &emsp;
                            </a>
                            <a href="https://opencollective.com/material-ui">
                                &emsp;
                                <img src={process.env.PUBLIC_URL + '/images/materialui-logo.jpg'} alt="Material-UI Logo" width={50} height={50}/>
                                &emsp;
                            </a>
                            <a href="https://www.npmjs.com/package/react-usa-map">
                                &emsp;
                                <img src={process.env.PUBLIC_URL + '/images/npm-logo.jpg'} alt="NPM Logo" width={50} height={50}/>
                                &emsp;
                            </a>
                            <a href="https://www.npmjs.com/package/@weknow/react-bubble-chart-d3">
                                &emsp;
                                <img src={process.env.PUBLIC_URL + '/images/npm-logo.jpg'} alt="NPM Logo" width={50} height={50}/>
                                &emsp;
                            </a>
                            <a href="https://www.npmjs.com/package/react-easy-bar-chart/v/0.0.2">
                                &emsp;
                                <img src={process.env.PUBLIC_URL + '/images/npm-logo.jpg'} alt="NPM Logo" width={50} height={50}/>
                                &emsp;
                            </a>
                        </li>
                        </ul>
                    </div>
                </div>
            </div>
        </Grid>
        <Grid item xs={12}>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="card-body">
                        <h5 class="card-title">test.py Results</h5>
                        <p>Results: {this.state.output}</p>
                    </div>
                </div>
            </div>
        </Grid>
      </Grid>
    </div>
        );
    }
}

export default AboutGrid;
// const useStyles = makeStyles((theme) => ({
//   root: {
//     flexGrow: 1,
//   },
//   paper: {
//     padding: theme.spacing(2),
//     textAlign: 'center',
//     color: theme.palette.text.secondary,
//   },
// }));

// let output = "outside of export block";

// export default function CenteredGrid() {
//   const classes = useStyles();
//   fetch("http:/results", {
//     method: "GET"}) //call the API
//     .then(response => output = response.text());