import React from 'react';
import EcoTable from "./EcoTable";

class Ecosystems extends React.Component{
    render() {
        return (
            <main role="main" class="inner cover">
            <div class="opaque">
            <h1 class="cover-heading">Ecosystems</h1>
            <p class="lead">Here you will find information about the types of ecosystems with endangered
            animals. This information will include facts like: prominent regions, global conservation
            status, if it is ruderal (thriving in disturbed areas), and if it is a wetland.</p>
            </div>
            <EcoTable />
            </main>
        );
    }
}

export default Ecosystems;