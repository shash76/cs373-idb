import React from 'react';
import { NavLink } from "react-router-dom";

class EcoEntry extends React.Component{
    constructor(props){
        super(props);
        this.name = new URLSearchParams(window.location.search);
        this.name = window.location.href.slice(window.location.href.indexOf('entry/') + 'entry/'.length);
        this.imgName = this.name.toString().toLowerCase().replace(/%20/g, "_").replace(/%27/g, "");
        this.state = {
            ecosystem: []
        }
        this.getAPIData();
    }

    getAPIData(){
        const APIurl = "/api/ecosystem?name=" + this.name;
        fetch(APIurl, {
            method: "GET"
        }) //call the API
        .then(response => response.json()) //turn content into JSON
        .then(data => this.setState({ecosystem: data.Ecosystem[0]})); //pass JSON data into animals array in state
    }

    render() {
        let ecoData = this.state.ecosystem;
        let imgName = this.imgName;
        return (
            <main role="main" class="inner cover">
                <h1 class="cover-heading">{ecoData.name}</h1>
                <div class="card mb-3">
                    <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src={process.env.PUBLIC_URL + `/images/${imgName}.jpg`} class="card-img" alt="Ecosystem"></img>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                        <h5 class="card-title">{ecoData.name}</h5>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><b>Countries: </b>
                                {ecoData.country ? ecoData.country.map(entry => {return(
                                <NavLink className = "nav-link" to={"/countries/entry/" + entry}>{entry}</NavLink>
                                );}) : null}
                                </li>  
                                <li class="list-group-item"><b>Global Conservation Status: </b>{ecoData.cons_status}</li>
                                <li class="list-group-item"><b>Is Ruderal: </b>{ecoData.ruderal ? 'Yes' : 'No'}</li>
                                <li class="list-group-item"><b>Is Wetland: </b>{ecoData.wetland ? 'Yes' : 'No'}</li>
                                <li class="list-group-item"><b>Latest Global Review of Endangerment: </b>{ecoData.year_review}</li>
                                <li class="list-group-item"><b>Summary: </b>{ecoData.description}</li>
                            </ul>
                        </div>
                    </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default EcoEntry;